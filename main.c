/*
	USB notifier program, capable of blinking more than one color
	
	Copyright (C) 2013 THK - realthk@gmail.com
	For USB notifier device from Dealextreme: http://dx.com/27062
	
	build and install:
		sudo apt-get install libusb-1.0-0-dev build-essential 
		make
		sudo make install

	Rather then run as root, better make device writeable to anyone with adding
		SUBSYSTEM=="usb", ATTRS{idVendor}=="0x1294" , ATTRS{idProduct}=="0x1320", MODE="0666"
	to any .rules file under /etc/udev/rules.d/	
	and reload udev rules with
		sudo udevadm control --reload

	Based on furyfire's version from 
	http://club.dx.com/forums/forums.dx/threadid.847809
	
	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <libusb.h>
#include <stdlib.h>
#include <stdbool.h>
#include <dirent.h>
#include <string.h>
#include <fcntl.h>
#include <signal.h> 
#include <unistd.h>

#define VID 0x1294
#define PID 0x1320


struct color_code {
	char *name;
	char code;
};

int set_color(char code) 
{
	struct libusb_device_handle *devh = NULL;	
	int ret;
	libusb_init( NULL );
	devh = libusb_open_device_with_vid_pid( NULL, VID, PID );
	if(devh == NULL) {
		perror( "Device not found\n" );
		return -1;
	}

	if(libusb_kernel_driver_active( devh, 0 )) {
		ret = libusb_detach_kernel_driver( devh, 0 );
		if(ret < 0) {
			perror( "Could not detach from kernel\n" );
			return -1;
		}
	}    
	
	char data[5];
	data[0] = code;
	data[1] = 0x4;
	data[2] = 0x4;
	data[3] = 0x4;
	data[4] = 0x4;

	int dummy;
	ret = libusb_interrupt_transfer( devh, 0x2, data, 5, &dummy, 0 );
	if(ret < 0) {
		perror( "Unknown error\n" );
		libusb_exit(NULL);	
		return -1;
	}
	libusb_close(devh);
	libusb_exit(NULL);

	return 0;
}


void blink_color(char* codes, int num, int* durations, int durationNum)
{
	int i = 0;
	int j = 0;
    // go on forever, with each color set for its required "duration" ms
	while (true) {
		set_color(codes[i++]);
		if (i==num)
			i=0;

		usleep(durations[j++] * 1000);
		
		if (j==durationNum)
			j=0;
	}
}

 
#define EXIT_SUCCESS 0  
#define EXIT_FAILURE 1  
  
static void daemonize(void)  
{  
    pid_t pid, sid;  
  
    /* Already a daemon? */  
    if ( getppid() == 1 ) return;  
  
    /* Fork off the parent process */  
    pid = fork();  
    if (pid < 0)    
    {     
        exit(EXIT_FAILURE);  
    }     
    
    if (pid > 0)    
    {     
        exit(EXIT_SUCCESS); /*Killing the Parent Process*/  
    }     

    umask(0);
  
    /* At this point we are executing as the child process */  
  
    /* Create a new SID for the child process */  
    sid = setsid();  
    if (sid < 0)    
    {  
        exit(EXIT_FAILURE);  
    }  
  
    if ((chdir("/")) < 0)  
    {  
        exit(EXIT_FAILURE);  
    }  
    
    close(STDIN_FILENO);
    close(STDOUT_FILENO);
    close(STDERR_FILENO);    
}  


int getUserId(char *s)
{
	int id = 0;
	char *p = strstr(s, "user/");
	if (p) {
		p += 5;
		char *pp = strchr(p, '.');
		if (pp) {
			*pp = '\0';
			id = atoi(p);
		}
	}
	
	return id;
}


int main( int argc, char** argv )
{
	int duration = 500;	
	struct color_code codes[] = { 
                {"off",                 0},
                {"black",               0},
                {"blink",               0},
                {"blue",                1},
                {"red",                 2},
                {"green",               3},
                {"aqua",                4},
                {"purple",              5},
                {"yellow",              6},
                {"white",               7},
	};
	
	// get basename (without path) of this program 
	char *cname = strrchr(argv[0], '/');
	if (cname)
		cname++;
	else
		cname = argv[0];	
	
	if(argc < 2) {
		printf( "Usage: %s [color] [color2 [color3] ... [duration] [duration2] [duration3] ...] \n", argv[0] );
		printf( "Where colors can be the following: \n" );
		int i;
		for (i=0; i<sizeof(codes)/sizeof(codes[0]); i++) {
			if (i)
				printf(", ");
			printf("%s", codes[i].name);
		}
		printf( "\n\nduration: how long a color should be lit (ms, default:%d) if more then one specified\n", duration );
		printf( "\nEg:\n    %s red green blue 250\n", cname );
		printf( " which results in blinking red/green/blue with each color lit for 250ms\n");
		printf( "\n\"off\", \"black\", \"blink\" are the same, so you may also write:\n" );		
		printf( "    %s red blink\n", cname );
		printf( "\nMust be either run as root for USB access or add \n" );
		printf( "SUBSYSTEM==\"usb\", ATTRS{idVendor}==\"%X\" , ATTRS{idProduct}==\"%X\", MODE=\"0666\"\n", VID, PID );
		printf( "to /etc/udev/rules.d/whatever.rules and then\nsudo udevadm control --reload\n" );
		return -1;
	}

	int ret;
	
	char fname[512];
	char buf[512];
	// get userId of currently running program
	sprintf(fname, "/proc/%d/cgroup", getpid());
	int fd;
	fd = open(fname, O_RDONLY);
	int n=read(fd, buf, 512);
	int userId = 0;
	userId = getUserId(buf);
	close(fd);
	// First find if process already run as daemon and kill it 
	DIR *dp;
	struct dirent *ep;     
	dp = opendir ("/proc");
	if (dp != NULL)
	{	
		while (ep = readdir (dp)) {
			int id = atoi(ep->d_name);
			// read all numeric directories under /proc, except current pid
			if (id && ep->d_type == DT_DIR && id!=getpid()) {
	  			// process name can be found in /proc/pid/comm
	  			sprintf(fname, "/proc/%s/comm", ep->d_name);
	  			fd = open(fname, O_RDONLY);
	  			n=read(fd, buf, 512);
	  			close(fd);
	  			if (n) {
	  				buf[n] = '\0';
	  				char *cp;
	  				// name might contain \n so remove it before comparing
	  				for (cp = buf; *cp; cp++) {
	  					if (*cp==13 || *cp==10)
	  						*cp = '\0';
	  				}
	  				
	  				// found it
	  				if (strcmp(buf, cname)==0) {
	  					// get userId of found other instance
			  			sprintf(fname, "/proc/%s/cgroup", ep->d_name);
						fd = open(fname, O_RDONLY);
						n=read(fd, buf, 512);
						close(fd);
						int otherUserId = 0;
						otherUserId = getUserId(buf);
	  					
	  					// if both have the same user, kill it and done
	  					if (otherUserId == userId) {
        					kill(id, SIGKILL);
        					break;
        				}
	  				}
	  			} 
	  		}
	  	}
		(void) closedir (dp);
	}	

	char codelist[512];
	int num = 0;
	char *p;
	int durations[512];
	int durationIndex = 0;
	for (n=1; n<argc; n++) {
		p = argv[n];

		int i;
		bool found = false; 
		for (i=0; i<sizeof(codes)/sizeof(codes[0]); i++) {
			if (strcmp(p, codes[i].name) == 0) {
				codelist[num++] = codes[i].code;
				
				found = true;
				break;
			} 
		}
		
		if (!found) {
			int n = atoi(p);
			if (n) {
				durations[durationIndex++] = n;
			}
		}
	}

	if (!num) {
		printf( "No valid color specified!\n" );
		set_color(0);
		return -1;
	} 	
	
	if (!durationIndex) {
		durations[durationIndex++] = duration;
	}
	
	// blinking requires daemonizing
	if (num > 1) {
		daemonize();
		blink_color(codelist, num, durations, durationIndex);  
	} else {
		// if no blinkinkg, simply set color and exit
		set_color(codelist[0]);
	}

	return EXIT_SUCCESS;
}

